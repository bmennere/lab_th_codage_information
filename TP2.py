from sklearn.cluster import KMeans
from sklearn.metrics import mean_squared_error
import numpy as np
import matplotlib.image as mpimg
import matplotlib.pyplot as plt

### TP2

small="C:/Users/user/Desktop/Projet_cours/3A/TCI/bird_small.tiff"
large="C:/Users/user/Desktop/Projet_cours/3A/TCI/bird_large.tiff"

##Question 2 et 3


""" On commence par mettre f sous la forme d'un tableau contenant tous les triplets RGB de chaque pixel"""
def reshape_f():
    img = mpimg.imread(small)   
    if img.dtype == np.float32: # Si le résultat n'est pas un tableau d'entiers
        img = (img * 255).astype(np.uint8)
    img=img/255
    tab=[]
    for i in range (128):
        for j in range (128):
            tab.append(img[i][j])
    return tab


X=reshape_f()

""" On peut ensuite exécuter l'algorithme k-means
On prend nombre de clusters = 10"""
kmeans = KMeans(n_clusters=10).fit(X)
labels= kmeans.labels_


""" A partire de là on va pouvoir reformer une image approximative"""
def recreate_image(codebook, labels, w, h):
    image = np.zeros((w, h, 3))
    label_idx = 0
    for i in range(w):
        for j in range(h):
            image[i][j] = codebook[labels[label_idx]]
            label_idx += 1
    return image

""" On recrée donc l'image approximative avec le nombre de clusters = 10 """
Y=recreate_image(kmeans.cluster_centers_, labels, 128, 128)
plt.figure(1)
plt.clf()
plt.axis('off')
plt.title('Quantized image (10 colors, K-Means)')
plt.imshow(Y)
plt.show()


## Question 4

""" On calcule la distorsion D(k) c'est à dire le MSE (Mean Square Error) entre f et f' """
def MSE(x,y):
    return mean_squared_error(x,y)


""" Pour calculer le MSE entre f et f' il faut d'abord mettre f' sous la même forme que f"""
def reshape_f_bis(Y):
    f_bis=[]
    for i in range(128):
        for j in range(128):
            f_bis.append(Y[i][j])
    return f_bis

print(" Mean Square Error entre f et f' : ",MSE(X,reshape_f_bis(Y)))


""" On programmme ensuite une fonction calculant le RLE d'une liste"""
def RLE(labels):
    val_en_cours=labels[0]
    cpt=1
    RLE=[]
    for i in range(1,len(labels)):
        if(labels[i]==val_en_cours):
            if(i==(len(labels))-1):
                cpt=cpt+1
                RLE.append(int(str(val_en_cours)+str(cpt)))
            else:
                cpt=cpt+1
        else:
            if(i==(len(labels)-1)):
                bp=(str(val_en_cours)+str(cpt))
                RLE.append(int(bp))
                cpt=1
                val_en_cours=labels[i]
                RLE.append(int(str(labels[i])+str(1)))
            else:
                bp=(str(val_en_cours)+str(cpt))
                RLE.append(int(bp))
                cpt=1
                val_en_cours=labels[i]
        
    return RLE


test=[9,9,9,3,4,6,6,7]
print("RLE de [9,9,9,3,4,6,6,7] :",RLE(test))

""" On calcule ensuite le code rate R(k)"""
def code_rate(labels):
    R=0
    nb_bits=0
    rle=RLE(labels)
    for i in range(len(rle)):
        nb_bits=nb_bits+(len(str(rle[i])))
    R=nb_bits/len(labels)
    return R

print("On teste avec le tableau précédent : ",code_rate(test))
print("On obtient bien le résultat attendu")

## Question 5

""" On va afficher un graphe qui montre l'évolution de D (la distorsion) en fonction de R (le coding rate)""" 
def D_in_function_R():
    D=[]
    R=[]
    for k in range(2,21):
        kmeans = KMeans(n_clusters=k).fit(X)
        labels= kmeans.labels_
        Y=recreate_image(kmeans.cluster_centers_, labels, 128, 128)
        D.append(MSE(X,reshape_f_bis(Y)))
        R.append(code_rate(labels))   
    return D,R

(d,r)=D_in_function_R()
plt.figure(1)
plt.title('Distorsion en fonction du coding rate')
plt.plot(r,d)
plt.show()    
        
            
            
        
        
        
    






























