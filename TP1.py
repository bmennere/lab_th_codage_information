import numpy as np
import random
import matplotlib.pyplot as plt
import image  



### TP Théorie et Codage de l'information ###

## TP 1 : Source Coding ##

# Question 1


""" On crée une fonction qui permet de calculer l'entropie (en bits/symboles) de données aléatoire"""
def entropy(x):
    entropy=0
    for i in range (len(x)):
        entropy-=x[i]*(np.log(x[i]))/(np.log(2))                  
    return entropy;

test=[0.5,0.2,0.1,0.2]
print("Entropie de [0.5,0.2,0.1,0.2] =",entropy(test)," bit/symbole")


#Question 2

""" On définit une fonction qui créer un jeu de données de la taille souhaité avec équiprobabilité de chaque bit"""
def tableau_equi(l):
    tableau=[]
    for i in range (l):
        tableau.append(random.randint(0,1))
    entropy_random=[0,0]
    for i in range (l):
        if tableau[i]==0:
            entropy_random[0]+=1
        else:
            entropy_random[1]+=1
    entropy_random[0]=entropy_random[0]/l
    entropy_random[1]=entropy_random[1]/l            
    return entropy_random


""" On vérifie que plus le jeu de données est long plus H se rapproche de 1
Ici, on est dans le cas de l'équiprobabilité des bits"""
print("Vérification H=1 bit")
print(entropy(tableau_equi(10))," bit/symbole")
print(entropy(tableau_equi(100)), "bit/symbole")
print(entropy(tableau_equi(1000)), "bit/symbole")
print(entropy(tableau_equi(10000)), "bit/symbole")
print(entropy(tableau_equi(100000)), "bit/symbole")
print("")

# Question 3

""" On crée une fonction permettant de visualiser la variation de l'entropie
en fonction de P[X=0]"""
def entropy_variation(nb_points):
    x=[]
    y=[]
    for i in range (1,nb_points):
        x.append(i/nb_points)
        y.append(entropy([x[i-1],1-x[i-1]]))
    plt.title("Variation of entropy in function of P[X=0]")
    plt.plot(x, y)
    plt.xlabel('P[X=0]')
    plt.ylabel('Entropy')
    plt.show()

""" On teste la fonction pour 1000 points"""
entropy_variation(1000)


# Question 4

def log2(x):
    return np.log(x)/np.log(2)

""" Fonction qui permet de calculer l'entropie d'une source avec mémoire
Les coefficients ont été calculés préalablement dans le cours"""
def entropy_memory(p00,p01):
    p10=1-p00
    p11=1-p01
    p0=(p01)/(1-p00+p01)
    p1=(1-p00)/(1-p00+p01)
    Hk=-(p00*log2(p00)+p10*log2(p10))*p0-(p01*log2(p01)+p11*log2(p11))*p1
    return Hk

print("Entropie avec mémoire avec p00=0.99 et p01=0.5 :  ",entropy_memory(0.99,0.5), " bit/symbole")


# Question 5


fichier="C:\\Users\\user\\Desktop\\Projet_cours\\3A\\TCI\\scan.png"


import matplotlib.image as mpimg

""" On commence par mettre les bits de l'image à 1 ou 0"""
img = mpimg.imread(fichier)   
if img.dtype == np.float32: # Si le résultat n'est pas un tableau d'entiers
    img = (img * 255).astype(np.uint8)
img=img/255

""" On met ensuite chaque pixel dans un nouveau tableau de longueur 3300*2550 (nombre de pixels de l'image)"""
tab=[]
for i in range(3300):
    for j in range(2550):
        if (img[i][j][0]==1):
            tab.append(1)
        else:
            tab.append(0)


""" On calcule ensuite la matrice de transition de l'image"""
def scan(tab):
    p00=0
    p01=0
    p10=0
    p11=0
    i=0
    etat=tab[0]
    char=0
    while i<3300*2550:
        char=tab[i]
        if (etat==0 and char==0):
            p00+=1
            etat=char
            i+=1
        elif (etat==0 and char==1):
            p01+=1
            etat=char
            i+=1
        elif (etat==1 and char==0):
            p10+=1
            etat=char
            i+=1
        elif (etat==1 and char==1):
            p11+=1
            etat=char
            i+=1
        else:
            i+=1
    nb_caractere=len(tab)
    print(nb_caractere)
    return [p00/(p00+p10),p10/(p00+p10),p01/(p01+p11),p11/(p01+p11)]


P=scan(tab)
print("Matrice de transition (p00, p10, p01, p11) : ",P)
"""On calcule ensuite l'entropie avec mémoire correspondant à cette image"""
print( "Entropie avec mémoire :" ,entropy_memory(P[0],P[2])," bit/symbole")


""" Fonction pour calculer l'entropie sans mémoire d'une source"""
def entropy_memoryless(p00,p01):
    p0=(p01)/(1-p00+p01)
    p1=(1-p00)/(1-p00+p01)
    Hk=-(p0*log2(p0))-(p1*log2(p1))
    return Hk

print("Entropie sans mémoire :",entropy_memoryless(P[0],P[2])," bit/symbole")
print("Nombre de bits par pixel, image de base : ", (108282*8)/(2550*3300))



    



    
