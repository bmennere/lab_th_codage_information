from sklearn.cluster import KMeans
from sklearn.metrics import mean_squared_error
import numpy as np
import matplotlib.image as mpimg
import matplotlib.pyplot as plt


### TP 3

small="C:/Users/user/Desktop/Projet_cours/3A/TCI/bird_small.tiff"

## Question 1


def reshape_f():
    img = mpimg.imread(small)   
    if img.dtype == np.float32: # Si le résultat n'est pas un tableau d'entiers
        img = (img * 255).astype(np.uint8)
    img=img
    tab=[]
    for i in range (128):
        for j in range (128):
            tab.append(img[i][j])
    return tab

X=reshape_f()


"""On commence par créer les 3 matrices de contingence des associées à chaque paire de couleurs"""
def couple_r_g_b(X):
    r_g= np.zeros((256,256))
    r_b= np.zeros((256,256))
    g_b= np.zeros((256,256))
    for i in range (len(X)):
        r_g[X[i][0]][X[i][1]] += 1
        r_b[X[i][0]][X[i][2]] += 1
        g_b[X[i][1]][X[i][2]] += 1
    return (r_g,r_b,g_b)



matrice_r_g_b=couple_r_g_b(X)

""" On calcule ensuite P(x) et P(y) pour chaque matrice de contingence"""
def vecteur_ligne_colonne(couple_r_g_b):
    Px_r_g=[]
    Py_r_g=[]
    Px_r_b=[]
    Py_r_b=[]
    Px_g_b=[]
    Py_g_b=[]
    Sx_r_g=0
    Sy_r_g=0
    Sx_r_b=0
    Sy_r_b=0
    Sx_g_b=0
    Sy_g_b=0
    for i in range(256):
        for j in range (256):
            Sx_r_g += couple_r_g_b[0][i][j]
            Sy_r_g += couple_r_g_b[0][j][i]
            Sx_r_b += couple_r_g_b[1][i][j]
            Sy_r_b += couple_r_g_b[1][j][i]
            Sx_g_b += couple_r_g_b[2][i][j]
            Sy_g_b += couple_r_g_b[2][j][i]
        Sx_r_g/=(256*256)
        Sy_r_g/=(256*256)
        Sx_r_b/=(256*256)
        Sy_r_b/=(256*256)
        Sx_g_b/=(256*256)
        Sy_g_b/=(256*256)
        Px_r_g.append(Sx_r_g)
        Py_r_g.append(Sy_r_g)
        Px_r_b.append(Sx_r_b)
        Py_r_b.append(Sy_r_b)
        Px_g_b.append(Sx_g_b)
        Py_g_b.append(Sy_g_b)
        Sx_r_g=0
        Sy_r_g=0
        Sx_r_b=0
        Sy_r_b=0
        Sx_g_b=0
        Sy_g_b=0
    return Px_r_g,Py_r_g,Px_r_b,Py_r_b,Px_g_b,Py_g_b




""" On calcule ensuite P(x,y)"""
def Px_Py(matrice_contingence):
    return matrice_contingence[0]/(256*256), matrice_contingence[1]/(256*256), matrice_contingence[2]/(256*256)
    #return matrice_contingence[0]/(128*128), matrice_contingence[1]/(128*128), matrice_contingence[2]/(128*128) 


#print("test test : ", (Px_Py(matrice_r_g_b)[0][128]))

""" On redéfinit log en base 2"""
def log2(x):
    return np.log(x)/np.log(2)


""" On peut maintenant calculer l'information mutuelle pour les 3 paires de couleurs"""
def mutual_information_discrete(Px_Py, vecteur_ligne_colonne):
    I_r_g=0
    I_r_b=0
    I_g_b=0
    for k in range (256):
        for m in range (256):
            if ( vecteur_ligne_colonne[0][k] != 0 and  vecteur_ligne_colonne[1][m] !=0 and Px_Py[0][k][m] != 0):
                I_r_g += Px_Py[0][k][m] * log2( (Px_Py[0][k][m]) / (vecteur_ligne_colonne[0][k] * vecteur_ligne_colonne[1][m]))
            if ( vecteur_ligne_colonne[2][k] != 0 and  vecteur_ligne_colonne[3][m] !=0 and Px_Py[1][k][m] != 0):
                I_r_b += Px_Py[1][k][m] * log2( (Px_Py[1][k][m]) / (vecteur_ligne_colonne[2][k] * vecteur_ligne_colonne[3][m]))
            if ( vecteur_ligne_colonne[4][k] != 0 and  vecteur_ligne_colonne[5][m] !=0 and Px_Py[2][k][m] != 0):
                I_g_b += Px_Py[2][k][m] * log2( (Px_Py[2][k][m])/ (vecteur_ligne_colonne[4][k] * vecteur_ligne_colonne[5][m]))
    return (I_r_g, I_r_b, I_g_b)



def plus_bas(r_g,r_b,g_b):
    if(r_g < r_b ) :
        if(r_g < g_b ) :
            print (" L'information mutuelle est la plus basse pour la paire de couleurs rouge-vert")
        else :
            print (" L'information mutuelle est la plus basse pour la paire de couleurs bleu-vert")
    else :
        if ( r_b < g_b ) :
            print (" L'information mutuelle est la plus basse pour la paire de couleurs rouge_bleu")
        else:
            print (" L'information mutuelle est la plus basse pour la paire de couleurs bleu-vert")

            
Y=vecteur_ligne_colonne(matrice_r_g_b)
Z=Px_Py(matrice_r_g_b)
#print("PxPy : ", Z[0])
I = mutual_information_discrete(Z,Y)
print(I)
plus_bas(I[0],I[1],I[2])

















    

    

                   
        
